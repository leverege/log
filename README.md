
The Log library abstracts away much of the logging functionality, making it so that with one call, log statements get logged in multiple places. 

## Usage
```shell
npm install @leverege/log --save
```

By default, the log defaults to the EmptyLog, which no-ops all logging methods.
To install a console logger, in the main class call:
```javascript
const log = require( '@leverege/log' );
log.setAdapter( log.ConsoleLog() )
```

To install a bunyan logger, call:
```javascript
const bunyanLog = bunyan.createLogger( { ... } );
log.setAdapter( log.BunyanLog( bunyanLog ) );
```

## Automated Log Creation and Installation

If you want to install a log entirely from a configuration variable (JSON, file, or plain object) call:
```javascript
log.createAdapter(<config - object, JSON string, or file>, <params - object>, <install - boolean>)
```
This function will create an Adapter based on your configuration, can parameterize your configuration if you are using a string or file, and will automatically call setAdapter if install is not false.

a configuration may look like:
```javascript
const logFile = '/var/config/transponder/logConfig.json'
```
or 
```javascript
const logString = '{
	"type" : "bunyan",
	"name" : "${logName}",
	"streams" : [
		{ 
			"type" : "rotating-file",
			"period" : "1d",
			"count" : 3,
			"level" : "warn",
			"path" : "./logs3/${logName}"
		}, 
		{ 
			"type" : "stream",
			"logName" : "${resource}"
		}
	]
}'
```
or
```javascript
const logObject = {
	type: 'console',
	mod: 'transponder'
}
```

In order to make the second JSON string configuration work, a parameters object would be supplied like so:
```javascript
const logParams = {
  logName: ClusterManager.logName( 'transponder' ),
  resource: `transponder-${process.env.TRANSPONDER_RUN_MODE || 'all'}`,
  logServiceAccount: process.env.LOG_SERVICE_ACCOUNT
}
```

So the installation would look like the following:
```javascript
log.createAdapter(logString, params, true)
```

## Logging

To log, call:
```javascript
const log = require( '@leverege/log' );
log.trace( 'trace log' );
log.debug( 'debug log' );
log.info( 'info log' );
log.warn( 'warn log' );
log.fatal( 'fatal log' );
```

The log methods can take an object as the first argument. See bunyans log for more information.

Other methods include:
```
child, setLevel, getLevel, isTrace, isDebug, isInfo, isWarn, isError, isError, isFatal
```

## Thread

The log can support a concept of an execution thread. This uses the AsyncLocalStorage behind the
scenes to maintain information about the current execution thread. When a thread is started, it 
will record a thread starting message and when the thread completes, a stop or error message. By 
default, these values will be included in any log statement occuring within a thread:

| Key | Description |
|-----|-------------|
| tid | The execution uuid. If a thread is started within another thread, this id will be the string `{parent tid}:{current tid}` |
| tfs | The time in milliseconds since the last log.thread() was invoked |
| tfp | The time in milliseconds since the last trace, debug, info, warn, error function was invoked. |

To use thread, call:

```
log.thread( executionFunction, /* function args */ )
```

Any arguments after the `executionFunction` will supplied to the function.

To set options and include your own fields in all of the log messages, thread() can be invoked
with an options object:

```
log.thread( options, executionFunction, /* function args */ )
```

The options object has several options:

| Key      | Type          | Description |
|----------|---------------|-------------|
| prefix   | string | This string will prefix every log message invoked in the thread. Options to sub threads will override it. |
| idPrefix | string | This string will prefix every thead id. |
| startMsg | string or false | If false, the default start thread message will not be written. If its a string, that string will be the log msg. |
| stopMsg  | string or false | If false, the default stop thread message will not be written. If its a string, that string will be the log msg. |
| errorMsg | string or false | If false, the default error thread message will not be written. If its a string, that string will be the log msg. |
| level    | string | The level to record start and stop messages. default to 'debug' |
| errorLevel | string | The level to record the error stop messages. default to 'debug' |
| ...      | ... | Any other fields will be appended to every log message. Parent fields will be inherited by sub threads, with the subtreads overwritting any parent values for the duration of its execution. |
