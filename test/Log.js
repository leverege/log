/* eslint-disable no-unused-expressions */
const { expect } = require( 'chai' )
const log = require( '../src' )
const EmptyLog = require( '../src/EmptyLog' )
const { createSerializer } = require( '../src/Serializer' )

// The exptected supported log levels.
const LogLevels = [ 'trace', 'debug', 'info', 'warn', 'error', 'fatal' ]
const sleep = ( ms ) => {
  return new Promise( ( res ) => { setTimeout( res, ms ) } )
}
describe( '@leverege/log tests', () => {

  //----------------------------------------------------------------------
  describe( 'Default / EmptyLog Tests', () => {
    it( 'Check an EmptyLog is created by default', () => {
      expect( log.getType() ).to.equal( 'empty' )
    } )

    it( 'Expected default functions are present', () => {
      expect( typeof log.setAdapter ).to.equal( 'function' )
      expect( typeof log.configure ).to.equal( 'function' )

      expect( typeof log.trace ).to.equal( 'function' )
      expect( typeof log.debug ).to.equal( 'function' )
      expect( typeof log.info ).to.equal( 'function' )
      expect( typeof log.error ).to.equal( 'function' )
      expect( typeof log.err ).to.equal( 'function' )
      expect( typeof log.fatal ).to.equal( 'function' )
      expect( typeof log.warn ).to.equal( 'function' )

      expect( typeof log.setLevel ).to.equal( 'function' )
      expect( typeof log.getLevel ).to.equal( 'function' )
      expect( typeof log.isTrace ).to.equal( 'function' )
      expect( typeof log.isDebug ).to.equal( 'function' )
      expect( typeof log.isInfo ).to.equal( 'function' )
      expect( typeof log.isWarn ).to.equal( 'function' )
      expect( typeof log.isError ).to.equal( 'function' )
      expect( typeof log.isFatal ).to.equal( 'function' )
      expect( typeof log.child ).to.equal( 'function' )
      expect( typeof log.module ).to.equal( 'function' )

      // TODO do these need to be exposed here?
      expect( typeof log.EmptyLog ).to.equal( 'function' )
      expect( typeof log.ConsoleLog ).to.equal( 'function' )
    } )

    it( 'Can set and get levels', () => {
      const isEnabled = ( logger, level ) => {
        // Invoking this => isEnabled( log, 'debug' )
        // is equivalent => log.isDebug()
        const isFunction = `is${level.replace( /^./, level[0].toUpperCase() )}`
        return logger[isFunction]()
      }

      LogLevels.forEach( ( level ) => {
        log.setLevel( level )
        expect( log.getLevel() ).to.equal( level )
        expect( isEnabled( log, level ) ).to.equal( true )
      } )
    } )

    it( 'Verify setting an invalid log level has no effect', () => {
      expect( log.getLevel() ).to.equal( log.setLevel( 'bogus' ) )
    } )
  } )

  //----------------------------------------------------------------------
  describe( 'Local String Log Tests', () => {
    it( 'Can log at all expected levels', () => {
      LogLevels.forEach( ( level ) => {
        let logged = null
        log.setAdapter( {
          [level] : ( toLog ) => { logged = toLog },
        } )
        const testString = `Logged at level ${level}`
        log[level]( testString ) // equiv: log.info( testString )
        expect( logged ).to.equal( testString )
      } )
    } )
  } )

  //----------------------------------------------------------------------
  describe( 'ConsoleLog Tests', () => {

    it( 'Can configure to be a ConsoleLog', () => {
      log.configure( { type : 'console', level : 'info' } )
      expect( log.getType() ).to.equal( 'console' )
    } )

    it( 'Can enable all levels on a ConsoleLog', () => {
      log.setLevel( 'trace' )
      expect( log.isTrace() ).to.equal( true )
      expect( log.isDebug() ).to.equal( true )
      expect( log.isInfo() ).to.equal( true )
      expect( log.isWarn() ).to.equal( true )
      expect( log.isError() ).to.equal( true )
      expect( log.isFatal() ).to.equal( true )
    } )

    it( 'Can set all log levels', () => {
      LogLevels.forEach( ( level ) => {
        log.setLevel( level )
        expect( log.getLevel() ).to.equal( level )
        log[level]( `logging at ${level}` )
      } )
    } )

    it( 'Can setup a child log', () => {
      expect( typeof log.child ).to.equal( 'function' )
      expect( typeof log.child( 'myPinoModule' ) ).to.equal( 'object' )
    } )

    it( 'Can thread the console log', async () => {
      const arr = []
      const adapter = log.getAdapter()
      const record = ( ...args ) => { arr.push( args ) }
      const testLog = {
        ...EmptyLog(),
        getType() { return 'threadTest' },
      
        trace : record,
        debug : record,
        info : record,
        warn : record,
        error : record,
        fatal : record,
      }      
      log.setAdapter( testLog )
      log.setLevel( 'trace' )
      const sf2 = async ( ) => {
        log.info( 'Sub Sub 1' )
        await sleep( 200 )
        log.warn( { foo : 2 }, 'Sub Sub 2' )
      }

      const sf1 = async ( ) => {
        log.info( { valueId : 1 }, 'Sub 1' )
        await sleep( 100 )
        log.info( { valueId : 2 }, 'Sub 2' )
        await log.thread( { subOption : 'Sub Option Override', prefix : null, startMsg : false, stopMsg : false }, sf2 )
        await sleep( 200 )
        log.warn( { foo : 1 }, 'Sub done' )
      }
      
      await log.thread( { rootOption : 'Root Option', prefix : '>>>' }, async ( ) => {
        log.info( 'Starting Root Thread' )
        await sleep( 100 )
        log.info( 'Root 1' )
        log.info( new Error( 'ack' ), 'Root Error' )
        await log.thread( 
          { subOption : 'Sub Option', startMsg : 'SubThread started:', stopMsg : 'SubThread stopped:' }, 
          sf1 )
        await sleep( 200 )
        log.warn( { bar : 12 }, 'Root Done' )
      } )
      log.debug( 'non-threaded' )
      log.setAdapter( adapter )
      const tid = arr[0][0].tid
      expect( typeof tid ).to.equal( 'string' )
      expect( arr[0][1].startsWith( '>>>Thread started:' ) ).to.equal( true )
      expect( arr[0][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[1][1].startsWith( '>>>Starting Root Thread' ) ).to.equal( true )
      expect( arr[1][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[1][0].tid ).to.equal( tid )
      expect( arr[2][1].startsWith( '>>>Root 1' ) ).to.equal( true )
      expect( arr[2][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[2][0].tid ).to.equal( tid )
      expect( arr[3][1].startsWith( '>>>Root Error' ) ).to.equal( true )
      expect( arr[3][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[3][0].tid ).to.equal( tid )
      expect( arr[3][0].err && typeof arr[3][0].err === 'object' ).to.equal( true )
      expect( arr[3][0].err.message ).to.equal( 'ack' )

      const tid2 = arr[4][0].tid
      expect( arr[4][1].startsWith( '>>>SubThread started:' ) ).to.equal( true )
      expect( arr[4][0].tid.startsWith( `${tid}:` ) ).to.equal( true )
      expect( arr[4][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[4][0].subOption ).to.equal( 'Sub Option' )
      expect( arr[4][0].valueId ).to.equal( undefined )
      expect( arr[5][1].startsWith( '>>>Sub 1' ) ).to.equal( true )
      expect( arr[5][0].tid.startsWith( `${tid}:` ) ).to.equal( true )
      expect( arr[5][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[5][0].subOption ).to.equal( 'Sub Option' )
      expect( arr[5][0].valueId ).to.equal( 1 )
      expect( arr[6][1].startsWith( '>>>Sub 2' ) ).to.equal( true )
      expect( arr[6][0].tid.startsWith( `${tid}:` ) ).to.equal( true )
      expect( arr[6][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[6][0].subOption ).to.equal( 'Sub Option' )
      expect( arr[6][0].valueId ).to.equal( 2 )
      // No automatic start msg
      expect( arr[7][1].startsWith( 'Sub Sub 1' ) ).to.equal( true )
      expect( arr[7][0].tid.startsWith( `${tid2}:` ) ).to.equal( true )
      expect( arr[7][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[7][0].subOption ).to.equal( 'Sub Option Override' )
      expect( arr[7][0].valueId ).to.equal( undefined )
      expect( arr[7][0].foo ).to.equal( undefined )
      expect( arr[8][1].startsWith( 'Sub Sub 2' ) ).to.equal( true )
      expect( arr[8][0].tid.startsWith( `${tid2}:` ) ).to.equal( true )
      expect( arr[8][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[8][0].subOption ).to.equal( 'Sub Option Override' )
      expect( arr[8][0].foo ).to.equal( 2 )
      // no automatic end
      expect( arr[9][1].startsWith( '>>>Sub done' ) ).to.equal( true )
      expect( arr[9][0].tid.startsWith( `${tid}:` ) ).to.equal( true )
      expect( arr[9][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[9][0].subOption ).to.equal( 'Sub Option' )
      expect( arr[9][0].foo ).to.equal( 1 )
      expect( arr[10][1].startsWith( '>>>SubThread stopped:' ) ).to.equal( true )
      expect( arr[10][0].tid.startsWith( `${tid}:` ) ).to.equal( true )
      expect( arr[10][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[10][0].subOption ).to.equal( 'Sub Option' )
      expect( arr[10][0].foo ).to.equal( undefined )

      expect( arr[11][1].startsWith( '>>>Root Done' ) ).to.equal( true )
      expect( arr[11][0].tid ).to.equal( tid )
      expect( arr[11][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[11][0].subOption ).to.equal( undefined )
      expect( arr[11][0].foo ).to.equal( undefined )
      expect( arr[11][0].bar ).to.equal( 12 )

      expect( arr[12][1].startsWith( '>>>Thread stopped:' ) ).to.equal( true )
      expect( arr[11][0].tid ).to.equal( tid )
      expect( arr[12][0].rootOption ).to.equal( 'Root Option' )
      expect( arr[11][0].subOption ).to.equal( undefined )
      expect( arr[12][0].foo ).to.equal( undefined )

      expect( arr[13][0] ).to.equal( 'non-threaded' )

    } )

    it( 'Can control sub thread ids', async () => {
      const arr = []
      const adapter = log.getAdapter()
      const record = ( ...args ) => { arr.push( args ) }
      const testLog = {
        ...EmptyLog(),
        getType() { return 'threadTest' },
      
        trace : record,
        debug : record,
        info : record,
        warn : record,
        error : record,
        fatal : record,
      }      
      log.setAdapter( testLog )
      log.setLevel( 'trace' )
  
      await log.thread( { getSubThreadId : () => 'subThreadYay' }, async ( ) => {
        log.info( 'Root 1' )
        await log.thread( 
          {}, 
          () => {
            log.info( 'Sub 1' )
          } 
        )
      } )
      log.setAdapter( adapter )
      
      const rootThreadId = arr[0][0].tid
      expect( arr.length ).to.eql( 6 )
      expect( arr[2][0].tid ).to.eql( `${rootThreadId}:subThreadYay` )
    } )
  } )

  //----------------------------------------------------------------------
  describe( 'BunyanLog Tests', () => {
    before( 'Set the adapter to Empty; add a Bunyan creator to the factory', () => {
      const BunyanLogCreator = require( '../src/BunyanLogCreator' )
      log.configure( { type : 'empty' } )
      log.addCreator( BunyanLogCreator )
      log.configure( {
        type : 'bunyan',
        name : 'noname',
        level : 'debug',
        streams : [ { type : 'stream' } ]
      } )
    } )

    it( 'Can create a BunyanLog', () => {
      expect( log.getType() ).to.equal( 'bunyan' )
    } )

    it( 'Can enable all log levels on a BunyanLog', () => {
      expect( log.setLevel( 'trace' ) ).to.equal( 'trace' )
      expect( log.isTrace() ).to.equal( true )
      expect( log.isDebug() ).to.equal( true )
      expect( log.isInfo() ).to.equal( true )
      expect( log.isWarn() ).to.equal( true )
      expect( log.isError() ).to.equal( true )
      expect( log.isFatal() ).to.equal( true )
    } )

    it( 'Can set all log levels', () => {
      LogLevels.forEach( ( level ) => {
        log.setLevel( level )
        expect( log.getLevel() ).to.equal( level )
      } )
    } )

    it( 'Verify setting an invalid log level has no effect', () => {
      expect( log.getLevel() ).to.equal( log.setLevel( 'bogus' ) )
    } )

    it( 'Stackdriver support removed', () => {
      const catcher = () => {
        const logConfig = {
          type : 'bunyan',
          name : 'whatever',
          level : 'info',
          streams : [ {
            type : 'stackdriver',
            logName : 'whocares',
          } ],
        }
        log.configure( logConfig, { logName : logConfig.name } )
      }
      expect( catcher ).to.throw( 'unknown stream type "stackdriver"' )
    } )

    it( 'Change the adapter to a Bunyan Rotating log type', () => {
      process.env.LOG_DIR = '/tmp'
      log.configure( {
        type : 'bunyan-rotating',
        level : 'info',
        name : 'rotating'
      } )
      expect( log.getType() ).to.equal( 'bunyan' )
    } )
  } )

  //----------------------------------------------------------------------
  describe( 'PinoLog Tests', () => {

    before( 'Set the adapter to PinoLogCreator', () => {
      const PinoLogCreator = require( '../src/PinoLogCreator' )
      log.configure( { type : 'empty' } )
      log.addCreator( PinoLogCreator )
      log.configure( { type : 'pino', name : 'noname' } )
    } )

    it( 'Can create a PinoLog', () => {
      expect( log.getType() ).to.equal( 'pino' )
    } )

    it( 'Can enable all levels on a PinoLog', () => {
      log.setLevel( 'trace' )
      expect( log.isTrace() ).to.equal( true )
      expect( log.isDebug() ).to.equal( true )
      expect( log.isInfo() ).to.equal( true )
      expect( log.isWarn() ).to.equal( true )
      expect( log.isError() ).to.equal( true )
      expect( log.isFatal() ).to.equal( true )
    } )

    it( 'Can disable all levels on a PinoLog', () => {
      log.setLevel( 'silent' )
      expect( log.isTrace() ).to.equal( false )
      expect( log.isDebug() ).to.equal( false )
      expect( log.isInfo() ).to.equal( false )
      expect( log.isWarn() ).to.equal( false )
      expect( log.isError() ).to.equal( false )
      expect( log.isFatal() ).to.equal( false )
    } )

    it( 'Can set all log levels', () => {
      LogLevels.forEach( ( level ) => {
        log.setLevel( level )
        expect( log.getLevel() ).to.equal( level )
        log[level]( `logging at ${level}` )
        log[level]( 12, `logging at ${level}` )
        log[level]( )
        log[level]( { foo : 12 } )
        log[level]( { foo : 12 }, 'foo' )
        log[level]( { foo : 12 }, 'foo', 'bar' )
      } )
    } )

    it( 'Can can thread', async () => {
      log.setLevel( 'trace' )
      const sf2 = async ( ) => {
        log.info( '!!! Subthread N' )
        await sleep( 200 )
        log.warn( { foo : 1 }, 'Subthread N done' )
      }

      const sf1 = async ( ) => {
        log.info( '!!! Subthread 1' )
        await sleep( 100 )
        log.info( 'did some extra work' )
        await log.thread( sf2 )
        await sleep( 200 )
        log.warn( { foo : 1 }, 'Subthread 1 done' )
      }
      
      const f = async ( ) => {
        log.info( 'Starting process' )
        await sleep( 100 )
        log.info( 'did some work' )
        await log.thread( sf1 )
        log.info( new Error( 'ack' ), 'Test an error' )
        await sleep( 200 )
        log.warn( { foo : 1 }, 'oh no' )
      }

      await log.thread( f )
    } )

    it( 'Can setup a child log', () => {
      expect( typeof log.child ).to.equal( 'function' )
      expect( typeof log.child( 'myPinoModule' ) ).to.equal( 'object' )
    } )

    it( 'Child logs rebind to adapter', () => {
      const oa = log.getAdapter()
      let a1c = 0
      let a2c = 0
      const a1 = { 
        ...log.ConsoleLog(), 
        child : ( name ) => {
          return { 
            ...log.ConsoleLog().child( name ), 
            info : ( ) => { a1c++ } 
          }
        },
        info : ( ) => { a1c++ } 
      }
      const a2 = { 
        ...log.ConsoleLog(),  
        child : ( name ) => {
          return { 
            ...log.ConsoleLog().child( name ), 
            info : ( ) => { a2c++ } 
          }
        },
        info : ( ) => { a1c++ } 
      }
      log.setAdapter( a1 )
      log.info()
      expect( a1c ).to.equal( 1 )
      const c = log.child( 'foo' )
      // console.log( 'indo=', c.child.info.toString() )
      c.info()
      expect( a1c ).to.equal( 2 )
      expect( a2c ).to.equal( 0 )
      log.setAdapter( a2 )
      c.info()
      expect( a1c ).to.equal( 2 )
      expect( a2c ).to.equal( 1 )
      log.setAdapter( a1 )
      c.info()
      expect( a1c ).to.equal( 3 )
      expect( a2c ).to.equal( 1 )
      log.setAdapter( oa )
    } )

    it( 'Change the adapter to a Pino Bogus log type', () => {
      const catcher = () => {
        const logConfig = {
          type : 'pino-bogus',
          level : 'info',
          name : 'bogus',
        }
        log.configure( logConfig, { logName : logConfig.name } )
      }
      expect( catcher ).to.throw( 'Unknown logger type pino-bogus, default to EmptyLog' )
    } )

  } )
  
  //----------------------------------------------------------------------
  describe( 'ServerLog Tests', () => {
    const BunyanConfig = {
      type : 'bunyan',
      name : 'noname',
      streams : [ { type : 'stream' } ]
    }
    const PinoConfig = {
      type : 'pino',
      name : 'noname'
    }

    before( 'Set the adapter to EmptyLog/ConsoleLog only', () => {
      log.configure( { type : 'empty' }, { clearLoggers : true } )
    } )

    it( 'Bunyan logs fail', () => {
      const catcher = () => {
        log.configure( BunyanConfig, { logName : 'bunyanName' } )
      }
      expect( catcher ).to.throw( 'Unknown logger type bunyan, default to EmptyLog' )
    } )

    it( 'Pino logs fail', () => {
      const catcher = () => {
        log.configure( PinoConfig, { logName : 'pinoName' } )
      }
      expect( catcher ).to.throw( 'Unknown logger type pino, default to EmptyLog' )
    } )

    it( 'Load the server loggers into the factory', () => {
      const ServerLog = require( '../src/ServerLog' )
      ServerLog.setLevel( 'warn' )
      expect( log.getLevel() ).to.equal( 'warn' )
    } )

    it( 'Create a bunyan adapter but do not install it', () => {
      log.configure( BunyanConfig, { install : false } )
      expect( log.getType() ).to.equal( 'empty' )
    } )

    it( 'Create and install a bunyan adapter', () => {
      log.configure( BunyanConfig )
      expect( log.getType() ).to.equal( 'bunyan' )
    } )

    it( 'Create a pino adapter but do not install it', () => {
      const adapter = log.configure( PinoConfig, { install : false } )
      expect( adapter.getType() ).to.equal( 'pino' )
      expect( log.getType() ).to.equal( 'bunyan' )
    } )

    it( 'Create a pino adapter', () => {
      log.configure( PinoConfig )
      expect( log.getType() ).to.equal( 'pino' )
    } )

    it( 'Still supports the deprecated createAdapter interface', () => {
      log.createAdapter( { logConfig : { type : 'console' } }, {}, true )
    } )
  } )

  describe( 'Serializer tests', () => {
    it( 'Creates a default serializer', () => {
      const serializer = createSerializer()

      const logMessage = {
        message : 'This should be stripped',
        url : 'http://localhost:8181',
        method : 'POST',
        user : {
          id : 'userId'
        },
        headers : {
          'user-agent' : 'Mozilla'
        },
        deep : {
          path : 'stripped as well'
        }
      }

      const res = serializer( logMessage )

      expect( res.message ).to.be.undefined
      expect( res.deep?.path ).to.be.undefined
      expect( res.url ).to.equal( logMessage.url )
      expect( res.method ).to.equal( logMessage.method )
      expect( res.user.id ).to.equal( logMessage.user.id )
      expect( res.headers['user-agent'] ).to.equal( logMessage.headers['user-agent'] )
    } )

    it( 'Creates a custom serializer using paths', () => {
      const serializer = createSerializer( { paths : [ 'url' ] } )
  
      const logMessage = {
        message : 'This should be stripped',
        url : 'http://localhost:8181',
        method : 'POST',
        user : {
          id : 'userId'
        },
        headers : {
          'user-agent' : 'Mozilla'
        },
        deep : {
          path : 'stripped as well'
        }
      }
  
      const res = serializer( logMessage )
  
      expect( res.url ).to.equal( logMessage.url )
  
      expect( res.message ).to.be.undefined
      expect( res.deep?.path ).to.be.undefined
      expect( res.method ).to.be.undefined
      expect( res.user?.id ).to.be.undefined
      expect( res.headers?.['user-agent'] ).to.be.undefined
    } )

    it( 'Creates a custom serializer using a function', () => {
      const serializer = createSerializer( obj => ( { url : obj.url } ) )
  
      const logMessage = {
        message : 'This should be stripped',
        url : 'http://localhost:8181',
        method : 'POST',
        user : {
          id : 'userId'
        },
        headers : {
          'user-agent' : 'Mozilla'
        },
        deep : {
          path : 'stripped as well'
        }
      }
  
      const res = serializer( logMessage )
  
      expect( res.url ).to.equal( logMessage.url )
  
      expect( res.message ).to.be.undefined
      expect( res.deep?.path ).to.be.undefined
      expect( res.method ).to.be.undefined
      expect( res.user?.id ).to.be.undefined
      expect( res.headers?.['user-agent'] ).to.be.undefined
    } )
  } )
} )
