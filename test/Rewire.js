const { expect } = require( 'chai' )
const rewire = require( 'rewire' )

const LogFactory = rewire( '../src/LogFactory' )
const expand = LogFactory.__get__( 'expand' ) // eslint-disable-line no-underscore-dangle
const paramReplace = LogFactory.__get__( 'paramReplace' ) // eslint-disable-line no-underscore-dangle

describe( '@leverege/log rewire unexported tests', () => {
  const TestConfigObject = {
    type : 'pino',
    name : '${logName}', // eslint-disable-line no-template-curly-in-string
    level : 'debug',
  }
  const TestConfigString = JSON.stringify( TestConfigObject )

  //----------------------------------------------------------------------
  describe( 'LogFactory paramReplace() tests', () => {
    it( 'Check the original is not effected by invoking', () => {
      const logConfig = { ...TestConfigObject }
      const results = paramReplace( logConfig )
      expect( results ).to.equal( TestConfigString )
      expect( TestConfigObject ).to.eql( logConfig )
    } )

    it( 'Check no params means we just get a stringified copy out', () => {
      const logConfig = { ...TestConfigObject }
      const results = paramReplace( logConfig )
      expect( results ).to.eql( TestConfigString )
    } )

    it( 'Check that the logName will be properly replaced', () => {
      const params = { logName : 'myTestLogName' }
      const results = paramReplace( TestConfigObject, params )
      expect( results ).to.not.eql( TestConfigString )
      const resultsObject = JSON.parse( results )
      expect( resultsObject.name ).to.equal( params.logName )
    } )
  } )

  //----------------------------------------------------------------------
  describe( 'LogFactory expand() tests', () => {
    it( 'Check a log config object does not get changed', () => {
      const logConfig = { ...TestConfigObject }
      const results = expand( logConfig, { logName : 'noEffect' } )
      expect( results ).to.equal( logConfig )
      expect( results ).to.eql( { ...TestConfigObject } )
    } )

    it( 'Check a string log config yields a copy of the original object', () => {
      const logConfig = TestConfigString
      const results = expand( logConfig )
      expect( results ).to.eql( { ...TestConfigObject } )
    } )

    it( 'Check a string log config had its parameter replaced', () => {
      const logConfig = TestConfigString
      const logName = 'replacedName'
      const results = expand( logConfig, { logName } )
      expect( results ).to.not.eql( { ...TestConfigObject } )
      expect( results.name ).to.equal( logName )
    } )
  } )
} )
