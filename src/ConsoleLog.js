const LEVELS = {
  trace : 10,
  debug : 20,
  info : 30,
  warn : 40,
  error : 50,
  fatal : 60
}

function print( prefix, args ) {
  let arr = args
  if ( prefix ) {
    arr = [ prefix ].concat( args )
  }
  console.log( ...arr ) // eslint-disable-line
}

let level = LEVELS.info

function create( mod ) {
  const log = {
    getType() { return 'console' },
    trace( ...args ) { if ( log.isTrace() ) print( mod, args ) },
    debug( ...args ) { if ( log.isDebug() ) print( mod, args ) },
    info( ...args ) { if ( log.isInfo() ) print( mod, args ) },
    warn( ...args ) { if ( log.isWarn() ) print( mod, args ) },
    error( ...args ) { if ( log.isError() ) print( mod, args ) },
    fatal( ...args ) { if ( log.isFatal() ) print( mod, args ) },
    child( m ) { return create( mod ? `${mod}:${m}` : m ) },
    isTrace() { return level <= LEVELS.trace },
    isDebug() { return level <= LEVELS.debug },
    isInfo() { return level <= LEVELS.info },
    isWarn() { return level <= LEVELS.warn },
    isError() { return level <= LEVELS.error },
    isFatal() { return level <= LEVELS.fatal },
    getLevel() {
      const l = level
      if ( l <= 10 ) { return 'trace' }
      if ( l <= 20 ) { return 'debug' }
      if ( l <= 30 ) { return 'info' }
      if ( l <= 40 ) { return 'warn' }
      if ( l <= 50 ) { return 'error' }
      return 'fatal'
    },
    setLevel( l ) {
      const nl = LEVELS[l]
      if ( nl ) {
        level = nl
        return log.getLevel()
      }
      return log.getLevel()
    }
  }
  return log
}

module.exports = create
