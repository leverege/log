function create( log ) {
  const combine = ( level, args ) => {
    if ( log.isLevelEnabled( level ) ) {
      if ( args[0] != null && typeof args[0] === 'object' ) {
        if ( args.length > 2 ) {
          log[level]( args[0], args.slice( 1 ).join( ' ' ) )
        } else {
          log[level]( args[0], args[1] )
        }
      } else if ( args.length > 1 ) {
        log[level]( args.join( ' ' ) )
      } else {
        log[level]( args[0] )
      }
    }
  }

  const getType = () => { return 'pino' }

  const trace = ( ...args ) => combine( 'trace', args )
  const debug = ( ...args ) => combine( 'debug', args )
  const info = ( ...args ) => combine( 'info', args )
  const warn = ( ...args ) => combine( 'warn', args )
  const error = ( ...args ) => combine( 'error', args )
  const fatal = ( ...args ) => combine( 'fatal', args )
  const child = ( name ) => { return create( log.child( { module : name } ) ) }

  const isTrace = () => { return log.isLevelEnabled( 'trace' ) }
  const isDebug = () => { return log.isLevelEnabled( 'debug' ) }
  const isInfo = () => { return log.isLevelEnabled( 'info' ) }
  const isWarn = () => { return log.isLevelEnabled( 'warn' ) }
  const isError = () => { return log.isLevelEnabled( 'error' ) }
  const isFatal = () => { return log.isLevelEnabled( 'fatal' ) }

  const getLevel = () => { return log.level }
  const setLevel = ( level ) => {
    if ( level ) { 
      log.level = level 
    }
    return getLevel()
  }

  return {
    getType,

    trace,
    debug,
    info,
    warn,
    error,
    fatal,
    module : child,
    child,
    setLevel,
    getLevel,
    isTrace,
    isDebug,
    isInfo,
    isWarn,
    isError,
    isFatal
  }
}

module.exports = create
