const Path = require( '@leverege/path' )

const DEFAULT_PATHS = [
  'url',
  'method',
  'user/id',
  'headers/user-agent'
]

exports.createSerializer = ( opts ) => {
  if ( typeof opts === 'function' ) {
    return opts
  }

  if ( typeof opts !== 'object' ) {
    opts = {}
  }
  
  const { paths } = opts
  
  let pathsToLog
  if ( paths && paths.length > 0 ) {
    pathsToLog = paths.map( path => new Path( path ) )
  } else {
    pathsToLog = DEFAULT_PATHS.map( path => new Path( path ) )
  }

  return ( logObj ) => {
    const ret = {}
    let hasKeys = false

    for ( let i = 0; i < pathsToLog.length; i++ ) {
      const path = pathsToLog[i]
      const val = path.get( logObj )

      if ( val != null ) {
        hasKeys = true
        path.set( ret, val )
      }
    }

    if ( !hasKeys ) {
      return undefined
    }

    return ret
  }
}
