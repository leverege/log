const bunyan = require( 'bunyan' )

function expandStreams( streams ) {
  return streams && streams.map( ( stream ) => {
    switch ( stream.type ) {
      case 'stream': {
        const logWithSeverity = () => {
          return {
            write : ( logStream ) => {
              const logOutput = JSON.parse( logStream )
              logOutput.severity = bunyan.nameFromLevel[logOutput.level].toUpperCase()
              process.stdout.write( `${JSON.stringify( logOutput )}\n` )
            }
          }
        }
        return {
          stream : logWithSeverity()
        }
      }
      case 'rotating-file':
      default:
        return stream
    }
  } )
}

function create( options, params = {} ) {
  const BunyanLog = require( './BunyanLog' )
  const serializers = params.serializers || bunyan.stdSerializers
  const name = params.logName || options.logName || 'unspecified'
  const opts = { name, serializers, ...options }

  // Canned stream setup for rotating
  if ( opts.type === 'bunyan-rotating' ) {
    opts.streams = [ {
      type : 'rotating-file',
      period : '1d',
      count : 3,
      level : process.env.LOG_LEVEL || opts.level || 'warn',
      path : `${process.env.LOG_DIR || './logs'}/${params.logName}.log`
    } ]
  }

  opts.streams = expandStreams( opts.streams )

  const bunyanLog = bunyan.createLogger( opts )
  return BunyanLog( bunyanLog )
}

module.exports = {
  create,
  canCreate() { return [ 'bunyan', 'bunyan-rotating' ] },
}
