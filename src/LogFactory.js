const Err = require( '@leverege/error' )

const EmptyLog = require( './EmptyLog' )
const ConsoleLog = require( './ConsoleLog' )

const loggers = {}

function paramReplace( optionsString, params ) {
  let optString = optionsString
  if ( !( typeof optString === 'string' ) && !( optString instanceof String ) ) {
    optString = JSON.stringify( optString )
  }
  if ( params ) {
    Object.keys( params ).forEach( ( key ) => {
      if ( typeof params[key] === 'string' || params[key] instanceof String ) {
        optString = optString.replace( new RegExp( `\\$\\{${key}\\}`, 'g' ), params[key] ) // eslint-disable-line security/detect-non-literal-regexp
      }
    } )
  }
  return optString
}

function expand( logConfig, params ) {

  if ( typeof logConfig === 'string' ) {
    // Try JSON
    try {
      const jsonConfig = paramReplace( logConfig, params )
      const cOpts = JSON.parse( jsonConfig )
      if ( cOpts != null && typeof cOpts === 'object' && !Array.isArray( cOpts ) ) {
        return { ...cOpts }
      }
    } catch ( x ) {
      if ( logConfig.trim().startsWith( '{' ) ) {
        console.log( { err : x }, 'Expanding config as JSON failed' ) // eslint-disable-line
      }
    }
  }

  return logConfig
}

function create( logFunctions ) {
  function addCreator( creator ) {
    if ( !creator || typeof creator.canCreate !== 'function' ) {
      throw Err.illegalArgument(
        'Attempted to add an invalid creator to the factory' )
    }

    creator.canCreate().forEach( ( logType ) => {
      loggers[logType] = creator
    } )
  }

  function configure(
    logConfig,
    params = { install : true, clearLoggers : false, }
  ) {
    const opts = expand( logConfig, params )

    if ( params.clearLoggers ) { // this is only intended for testing
      Object.keys( loggers ).forEach( key => delete loggers[key] )
    }

    let log

    if ( opts.type === 'empty' ) {
      log = EmptyLog()
    } else if ( opts.type === 'console' ) {
      // TODO: figure out opts.mod and logConfig for ConsoleLog
      log = ConsoleLog( opts && opts.mod )
      log.setLevel( opts.level || 'warn' ) // TODO: fix this cheat
    } else if ( loggers[opts.type] ) {
      log = loggers[opts.type].create( opts, params )
    } else {
      throw Err.illegalArgument(
        `Unknown logger type ${opts.type}, default to EmptyLog` )
    }

    if ( params.install !== false ) {
      logFunctions.setAdapter( log )
    }

    return log
  }

  /**
   * DEPRECATED
   */
  function createAdapter( options, params, install = true ) {
    const log = configure(
      options.logConfig || options,
      {
        install, ...params
      }
    )
    log.warn( 'DEPRECATED log.createAdater - please upgrade to latest @leverege/server' )
  }

  return {
    createAdapter, // DEPRECATED
    configure,
    addCreator,
  }
}

/**
 * Inits the log's adapter using the options and parameters. This is the
 * same as calling log.createAdapter( options, params ) in version 1.5
 */
function init( log, options, params ) {
  const c = create( log )
  c.configure( options, params )
}

module.exports = {
  create,
  init
}
