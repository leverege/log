const { AsyncLocalStorage } = require( 'async_hooks' )
const B62 = require( '@leverege/base62-util' )
const EmptyLog = require( './EmptyLog' )
const ConsoleLog = require( './ConsoleLog' )
const ChildLog = require( './ChildLog' )

let adapter = EmptyLog()
function setAdapter( a ) { 
  adapter = a || EmptyLog
}

function getAdapter() {
  return adapter
}

function getType( ) {
  return adapter && adapter.getType ? adapter.getType() : 'unknown'
}

function setLevel( level ) { return adapter.setLevel( level ) }
function getLevel( ) { return adapter.getLevel( ) }

function trace( ...args ) { contextualize( 'trace', ...args ) }
function debug( ...args ) { contextualize( 'debug', ...args ) }
function info( ...args ) { contextualize( 'info', ...args ) }
function warn( ...args ) { contextualize( 'warn', ...args ) }
function error( ...args ) { contextualize( 'error', ...args ) }
function fatal( ...args ) { contextualize( 'fatal', ...args ) }

function child( name ) { 
  return new ChildLog( module.exports, name )
}

function isTrace( ) { return adapter.isTrace( ) }
function isDebug( ) { return adapter.isDebug( ) }
function isInfo( ) { return adapter.isInfo( ) }
function isWarn( ) { return adapter.isWarn( ) }
function isError( ) { return adapter.isError( ) }
function isFatal( ) { return adapter.isFatal( ) }

const context = new AsyncLocalStorage()

function contextualize( call, ...args ) {
  const store = context.getStore()
  if ( store == null ) {
    // no context
    adapter[call]( ...args )
    return
  }
  const { tid, threadStart, prevTime, prefix, logOptions } = store
  const now = Date.now()
  if ( args[0] && typeof args[0] === 'object' && !Array.isArray( args[0] ) ) {
    const msg = args.slice( 1 )
    if ( prefix && prefix.length > 0 && msg.length > 0 && typeof msg[0] === 'string' ) {
      msg[0] = `${prefix}${msg[0]}`
    }
    let logObj = args[0]
    if ( logObj instanceof Error ) {
      logObj = { err : logObj }
    }

    adapter[call]( { 
      ...logOptions,
      ...logObj,
      tid, 
      tfs : now - threadStart, 
      tfp : now - prevTime,
    }, ...msg )
  } else {
    let msg = args
    if ( prefix && prefix.length > 0 && msg.length > 0 && typeof msg[0] === 'string' ) {
      msg = [ ...msg ]
      msg[0] = `${prefix}${msg[0]}`
    }
    adapter[call]( { 
      ...logOptions,
      tid : store.tid, 
      tfs : now - store.threadStart, 
      tfp : now - store.prevTime,
    }, ...msg )
  } 
  store.prevTime = now
  // store.step[store.step.length - 1] += 1
}

/**
 * Adds a key value pair to the current log Options. This will be included in all logs
 * @param {string} key the key of the option to set 
 * @param  {...any} value the value to set. If undefined, the key will be removed from the options 
 */
function setLogOption( key, value ) {
  const store = context.getStore()
  if ( store != null ) {
    if ( value === undefined ) {
      delete store.logOptions[key]
    } else {
      store.logOptions[key] = value
    }
  }

}
/**
 * Starts a log thread. This will use AsyncLocalStorage to supply a log thread id (tid),
 * time from start of the thread (tfs), time from previous log in the thread(tfp), the step
 * in the thread, and any optional values in opts. 
 * @param {Object} [opts] Optional arguments to include with every log in the thread
 * @param {string} [opts.prefix] If supplied, all msgs will be prefixed with this string.
 * @param {string} [opts.idPrefix] If supplied, the thread id will be prefixed with this string.
 * @param {string} [opts.startMsg] If false, no thread start msg will be logged. If its a string,
 * this string will be the used as the thread start message.
 * @param {string} [opts.stopMsg] If false, no thread end msg will be logged. If its a string,
 * this string will be the used as the thread end message.
 * @param {string} [opts.errorMsg] If false, no thread error msg will be logged. If its a string,
 * this string will be the used as the thread error message.
 * @param {string} [opts.level] the level at which the start/stop msgs will be logged (default: debug)
 * @param {string} [opts.errorLevel] the level at which the start/stop msgs will be logged (default: debug)
 * @param {function} func the function that executes in the log thread
 * @param  {...any} args arguments to invoke the function with
 * @returns {Object} a run context
 */
function thread( opts, func, ...args ) {
  const currentStore = context.getStore()
  let fn = func
  let options = opts
  let rest = args
  if ( typeof opts === 'function' ) {
    fn = opts
    options = {}
    rest = [ func, ...args ]
  }
  const { 
    prefix : remove,
    idPrefix = '', 
    startMsg = 'Thread started:', 
    stopMsg = 'Thread stopped:', 
    errorMsg = 'Thread Finished in error:', 
    level = 'debug', 
    errorLevel = 'debug',
    getSubThreadId : oGetSubThreadId,
    subThreadId : oSubThreadId,
    ...tOpts } = options
  const prefix = options.prefix === undefined ? currentStore?.prefix : options.prefix
  const now = Date.now()
  
  let tid
  if ( currentStore ) {
    const getSubThreadId = currentStore.getSubThreadId
    const subThreadId = currentStore.subThreadId
    let nSubThreadId
    if ( typeof subThreadId === 'string' ) {
      nSubThreadId = subThreadId
    } else if ( typeof getSubThreadId === 'function' ) {
      nSubThreadId = getSubThreadId( currentStore )
    } else {
      nSubThreadId = B62.v4()
    }
    tid = `${currentStore.tid}:${idPrefix || ''}${nSubThreadId}`
  } else {
    tid = ( idPrefix || '' ) + B62.v4()
  }
  const store = {
    logOptions : { ...currentStore?.logOptions, ...tOpts },
    getSubThreadId : oGetSubThreadId,
    subThreadId : oSubThreadId,
    tid,
    // step : currentStore ? [ ...currentStore.step, 0 ] : [ 0 ],
    threadStart : now,
    prevTime : now,
    prefix 
  }
  const f = async () => {
    if ( startMsg !== false ) {
      module.exports[level]( `${startMsg} ${tid}` )
    }
    try {
      const result = await fn( ...rest )
      if ( stopMsg !== false ) {
        module.exports[level]( `${stopMsg} ${tid}` )
      }
      return result
    } catch ( err ) {
      if ( errorMsg !== false ) {
        module.exports[errorLevel]( { err }, `${errorMsg} ${tid}` )
      }
      throw err
    }
  }
  return context.run( store, f, ...args )
}

module.exports = {
  EmptyLog,
  ConsoleLog,
  
  getAdapter,
  setAdapter, // used by main to set the log and output
  trace,
  debug,
  info,
  warn,
  error,
  err : error,
  fatal,
  module : child,
  child,
  setLevel,
  getLevel,
  getType,
  isTrace,
  isDebug,
  isInfo,
  isWarn,
  isError,
  isFatal,

  thread,
  setLogOption
}
