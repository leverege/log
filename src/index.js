const VERSION = require( '../package.json' ).version

// Get global context
let gl = null
if ( typeof global === 'object' ) {
  gl = global
} else if ( typeof window === 'object' ) {
  gl = window
}

if ( gl && gl.leveregeLog ) {
  // If a previous version of log exists, use it instead
  const note = `@leverege/log ${VERSION} using previously installed version ${gl.leveregeLog.version}`
  gl.leveregeLog.log.debug( note )
  module.exports = gl.leveregeLog.log
} else {
  const Log = require( './Log' )
  const Factory = require( './LogFactory' )

  module.exports = {
  
    ...Log,

    // In future remove this and have the main program use NodeFactory 
    // directly. LogFactory.init( log, options, params )
    ...Factory.create( Log )
  }
  // console.log( `Installing ${VERSION} of leverege/log` )
  gl.leveregeLog = {
    version : VERSION,
    log : module.exports
  }
}
