const BunyanLogCreator = require( './BunyanLogCreator' )
const PinoLogCreator = require( './PinoLogCreator' )

const Log = require( '.' )

Log.addCreator( BunyanLogCreator )
Log.addCreator( PinoLogCreator )

module.exports = Log
