const logger = require( 'pino' )
const { createSerializer } = require( './Serializer' )

function create( options, params = {} ) {
  const PinoLog = require( './PinoLog' )

  const serializers = {
    ...logger.stdSerializers
  }

  if ( !options.serializers || !options.serializers?.req ) {
    serializers.req = createSerializer()
  }

  if ( options.serializers ) {
    Object
      .keys( options.serializers )
      .forEach( ( key ) => {
        serializers[key] = createSerializer( options.serializers[key] )
      } )
  }

  const opts = {
    name : options.name,

    ...options,

    formatters : {
      level( label, number ) {
        return {
          level : number,
          severity : label.toUpperCase()
        }
      }
    },
    serializers
  }

  return PinoLog( logger( opts ) )
}

module.exports = {
  create,
  canCreate() { return [ 'pino' ] },
}
