function create( log ) {
  const bunyan = require( 'bunyan' )
  const getType = () => { return 'bunyan' }
  const LEVELS = {
    trace : bunyan.TRACE,
    debug : bunyan.DEBUG,
    info : bunyan.INFO,
    warn : bunyan.WARN,
    error : bunyan.ERROR,
    fatal : bunyan.FATAL
  }

  const trace = log.trace.bind( log )
  const debug = log.debug.bind( log )
  const info = log.info.bind( log )
  const warn = log.warn.bind( log )
  const error = log.error.bind( log )
  const fatal = log.fatal.bind( log )
  const child = ( name ) => { return create( log.child( { module : name } ) ) }
  const isTrace = () => { return log.level() <= bunyan.TRACE }
  const isDebug = () => { return log.level() <= bunyan.DEBUG }
  const isInfo = () => { return log.level() <= bunyan.INFO }
  const isWarn = () => { return log.level() <= bunyan.WARN }
  const isError = () => { return log.level() <= bunyan.ERROR }
  const isFatal = () => { return log.level() <= bunyan.FATAL }

  function getLevel() {
    return bunyan.nameFromLevel[log.level()]
  }

  function setLevel( l ) {
    log.level( LEVELS[l] || getLevel() )
    return getLevel()
  }

  return {
    getType,

    trace,
    debug,
    info,
    warn,
    error,
    fatal,
    module : child,
    child,
    setLevel,
    getLevel,
    isTrace,
    isDebug,
    isInfo,
    isWarn,
    isError,
    isFatal
  }
}

module.exports = create
