class ChildLog {

  constructor( log, name ) {
    this.log = log
    this.adapter = log.getAdapter()
    this.name = name
    this.child = this.adapter.child( name )
  }

  verify() {
    if ( this.adapter !== this.log.getAdapter() ) {
      this.adapter = this.log.getAdapter()
      this.child = this.adapter.child( this.name )
    }
  }

  getType() { return this.log.getType() }

  trace( ...args ) { this.verify(); this.child.trace( ...args ) }
  debug( ...args ) { this.verify(); this.child.debug( ...args ) }
  info( ...args ) { this.verify(); this.child.info( ...args ) }
  warn( ...args ) { this.verify(); this.child.warn( ...args ) }
  error( ...args ) { this.verify(); this.child.error( ...args ) }
  fatal( ...args ) { this.verify(); this.child.fatal( ...args ) }
  child( name ) { return this.log.child( `${this.name}.${name}` ) }
  isTrace() { return this.log.isTrace() }
  isDebug() { return this.log.isDebug() }
  isInfo() { return this.log.isInfo() }
  isWarn() { return this.log.isWarn() }
  isError() { return this.log.isError() }
  isFatal() { return this.log.isFatal() }
  getLevel() { return this.log.getLevel() }
  setLevel( l ) { this.log.setLevel( l ) }
}

module.exports = ChildLog
