const empty = () => { } 

const LEVELS = {
  trace : 10,
  debug : 20,
  info : 30,
  warn : 40,
  error : 50,
  fatal : 60
}

let level = LEVELS.warn

const log = {

  getType() { return 'empty' },

  trace : empty,
  debug : empty,
  info : empty,
  warn : empty,
  error : empty,
  fatal : empty,

  child() { return log },

  isTrace() { return level <= LEVELS.trace },
  isDebug() { return level <= LEVELS.debug },
  isInfo() { return level <= LEVELS.info },
  isWarn() { return level <= LEVELS.warn },
  isError() { return level <= LEVELS.error },
  isFatal() { return level <= LEVELS.fatal },

  getLevel() {
    const l = level
    if ( l <= 10 ) return 'trace'
    if ( l <= 20 ) return 'debug'
    if ( l <= 30 ) return 'info'
    if ( l <= 40 ) return 'warn'
    if ( l <= 50 ) return 'error'
    return 'fatal'
  },

  setLevel( l ) {
    level = LEVELS[l] || level
    return log.getLevel()
  }
}

module.exports = () => { return log }
