
const rewire = require( 'rewire' )

const Log = require( '../lib' )

const LogFactory = rewire( '../lib/LogFactory' )
const expand = LogFactory.__get__( 'expand' )
const paramReplace = LogFactory.__get__( 'paramReplace' )

const logit = (log,thing) => {
  console.dir( { log, thing }, { depth : 8, colors : true } )
}

let logAdapter

const TestConfigObject = {
  type : 'bunyan',
  name : '${logName}',
  level : 'warn',
  streams : [ { type : 'stream' } ],
}

const TestConfigString = JSON.stringify( TestConfigObject )

const TestConfigArray = [
  { type : 'pino' },
  { level : 'info' },
]

const init = ( logConfig, logName = '' ) => {
  logit( logConfig, `init.logConfig` )
  logAdapter = Log.createAdapter( logConfig, { logName } )
}

const initServerLogs = () => {
  require( '../lib/ServerLog' )
}

// pino logs
const initPinoLogs = ( level = 'debug' ) => {
  const logConfig = {
    type : 'pino',
    //  name : 'noname',
    prettyPrint : false,
    level,
    redact : [
      'secret',
      'password',
      'passwd',
    ]
  }
  init( logConfig )
}

// bunyan streaming logs
const initBunyanStream = ( level = 'debug' ) => {
  const logConfig = {
    type : 'bunyan',
    name : 'noname',
    level,
    streams : [ { type : 'stream' } ],
  }
  init( JSON.stringify( logConfig ) )
}

// bunyan rotating logs
const initBunyanRotate = ( level = 'debug' ) => {
  const logName = 'bunny-rot'
  const logConfig = {
    type : 'bunyan-rotating',
    name : logName,
    level,
  }
  init( logConfig, logName )
  console.log( 'Logs will go to the ./logs directory' )
}

// console
const initConsoleLog = ( level = 'debug' ) => {
  const logConfig = {
    type : 'console',
    level,
  }
  init( logConfig )
}

// empty
const initEmptyLog = ( level = 'debug' ) => {
  const logConfig = {
    type : 'empty',
    level,
  }
  init( logConfig )
}

const logStuff = () => {
  Log.setLevel('warn')
  Log.trace( 'logged at trace' )
  Log.debug( 'logged at debug' )
  Log.info( 'logged at info' )
  Log.warn( 'logged at warn' )
  Log.error( 'logged at error' )
  Log.fatal( 'logged at fatal' )

  Log.warn( {
    logme : 'log string',
    username : 'einstein',
    password : 'e=mc2',
    passwd : '2cm=e',
  } )
}

const addChild = ( prefix = 'childPrefix' ) => {
  Log.child( prefix )
}

console.log( '\n---------------------------------------------------' )
console.log( 'Run an init followed by the logStuff()' )
console.log( '  addChild()         - adds a child prefix?' )
console.log( '  initBunyanRotate() - inits for bunyan rotating logs' )
console.log( '  initBunyanStream() - inits for bunyan streams' )
console.log( '  initPinoLogs()     - inits for pino logging' )
console.log( '  initConsoleLog()   - inits for console logging' )
console.log( '  initEmptyLog()     - inits for empty logging' )
console.log( '  initServerLogs()   - inits the server side logging' )
console.log( '  logStuff()         - logs things' )
