
const log = require( '../src' )

const PinoLogCreator = require( '../src/PinoLogCreator' )
log.configure( { type : 'empty' } )
log.addCreator( PinoLogCreator )
log.configure( { type : 'pino', level : 'debug', name : 'noname' } )

const { createSerializer } = require( '../src/Serializer' )
const serializer = createSerializer()
const err = {
  message : 'This should be stripped',
  url : 'http://localhost:8181',
  method : 'POST',
  user : {
    id : 'userId'
  },
  headers : {
    'user-agent' : 'Mozilla'
  },
  deep : {
    path : 'stripped as well'
  }
}
